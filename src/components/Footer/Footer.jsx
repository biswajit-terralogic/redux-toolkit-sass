import React from "react";
import "./Footer.scss";

const Footer = () => {
  return (
    <div className="footer">
      <div>MovieKy</div>
      <div>Ⓒ2022, MovieKy, Inc. or its affiliates</div>
    </div>
  );
};

export default Footer;
