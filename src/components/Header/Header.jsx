import React from "react";
import { Link } from "react-router-dom";

import userImg from "../../images/user.png";
import "./Header.scss";

const Header = () => {
  return (
    <div className="header">
      <Link to="/">
        <div className="logo">MoviesKy</div>
      </Link>
      <div className="user-img">
        <img src={userImg} alt="user_img" />
      </div>
    </div>
  );
};

export default Header;
