import React from "react";

import MovieCard from "../MovieCard/MovieCard";
import "./MovieList.scss";

const MovieList = ({ movies = [] }) => {
  return (
    <div className="movie-wrapper">
      <div className="movie-list">
        <h2>Movies</h2>
        <div className="movie-container">
          {movies.map((movie) => (
            <MovieCard key={movie.imdbID} data={movie} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default MovieList;
