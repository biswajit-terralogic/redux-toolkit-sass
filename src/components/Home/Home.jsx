import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { fetchAllMovies, getAllMovies } from "../../features/movies/moviesSlice";
import MovieList from "../MoviesList/MovieList";
import Loader from "../Loader/Loader";

const Home = () => {
  const dispatch = useDispatch();
  const { movies, isLoading, error } = useSelector(getAllMovies);

  useEffect(() => {
    dispatch(fetchAllMovies());
  }, [dispatch]);

  console.log("error: ", error)
  
  return (
    <div>
      <div className="banner-img" />
      {!isLoading ? <MovieList movies={movies} /> : <Loader />}
    </div>
  );
};

export default Home;
