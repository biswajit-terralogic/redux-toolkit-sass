import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import {
  getDetails,
  fetchMovieDetails
} from "../../features/movies/movieDetailsSlice";
import Loader from "../Loader/Loader";

const MovieDetails = () => {
  const imdbID = window.location.pathname.split("/")[2];
  const dispatch = useDispatch();
  const { movieDetails, isLoading } = useSelector(getDetails);

  useEffect(() => {
    dispatch(fetchMovieDetails(imdbID))
  }, [imdbID, dispatch]);
  
  return (
    <div className="movie-section">
      {isLoading ? (
        <Loader />
      ) : (
        <>
          <div className="section-left">
            <div className="movie-title">{movieDetails.Title}</div>
            <div className="movie-rating">
              <span>
                IMDB Rating <i className="fa fa-star"></i> : {movieDetails.imdbRating}
              </span>
              <span>
                IMDB Votes <i className="fa fa-thumbs-up"></i> :{" "}
                {movieDetails.imdbVotes}
              </span>
              <span>
                Runtime <i className="fa fa-film"></i> : {movieDetails.Runtime}
              </span>
              <span>
                Year <i className="fa fa-calendar"></i> : {movieDetails.Year}
              </span>
            </div>
            <div className="movie-plot">{movieDetails.Plot}</div>
            <div className="movie-info">
              <div>
                <span>Director</span>
                <span>{movieDetails.Director}</span>
              </div>
              <div>
                <span>Stars</span>
                <span>{movieDetails.Actors}</span>
              </div>
              <div>
                <span>Generes</span>
                <span>{movieDetails.Genre}</span>
              </div>
              <div>
                <span>Languages</span>
                <span>{movieDetails.Language}</span>
              </div>
              <div>
                <span>Awards</span>
                <span>{movieDetails.Awards}</span>
              </div>
            </div>
          </div>
          <div className="section-right">
            <img src={movieDetails.Poster} alt={movieDetails.Title} />
          </div>
        </>
      )}
    </div>
  )
};

export default MovieDetails;
