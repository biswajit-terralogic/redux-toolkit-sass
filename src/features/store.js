import { configureStore } from "@reduxjs/toolkit";
import moviesReducer from "./movies/moviesSlice";
import movieDetailsReducer from "./movies/movieDetailsSlice";

export const store = configureStore({
  reducer: { movies: moviesReducer, details: movieDetailsReducer },
});
