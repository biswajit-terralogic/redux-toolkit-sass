import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import { apiKey } from "../../common/apis/apiKey";
import movieApi from "../../common/apis/movieApi";

export const fetchAllMovies = createAsyncThunk(
  "movies/fetchAllMovies",
  async (_, thunkAPI) => {
    try {
      const searchTerm = "Harry";
      const response = await movieApi.get(
        `?apiKey=${apiKey}&s=${searchTerm}&type=movie`
      );

      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error?.response?.data?.Error);
    }
  }
);

const initialState = {
  movies: [],
  isLoading: false,
  error: null,
};

const moviesSlice = createSlice({
  name: "movies",
  initialState,
  reducers: {},
  extraReducers: {
    [fetchAllMovies.pending]: (state, action) => {
      return { ...state, isLoading: true, error: null };
    },
    [fetchAllMovies.fulfilled]: (state, action) => {
      return {
        ...state,
        isLoading: false,
        error: null,
        movies: action.payload.Search,
      };
    },
    [fetchAllMovies.rejected]: (state, { payload }) => {
      return { ...state, isLoading: false, error: payload };
    },
  },
});

export const getAllMovies = (state) => state.movies;
export default moviesSlice.reducer;
