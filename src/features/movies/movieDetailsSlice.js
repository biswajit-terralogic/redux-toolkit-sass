import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import { apiKey } from "../../common/apis/apiKey";
import movieApi from "../../common/apis/movieApi";

const initialState = {
  movieDetails: {},
  isLoading: false,
  error: null,
};

export const fetchMovieDetails = createAsyncThunk("movies/fetchMovieDetails", async(imdbID) => {
  const response = await movieApi
          .get(`?apiKey=${apiKey}&i=${imdbID}`)
  return response.data;
})

const movieDetailsSlice = createSlice({
  name: "movieDetails",
  initialState,
  reducers: {},
  extraReducers: {
    [fetchMovieDetails.pending]: (state, action) => {
      return {...state, isLoading: true, error: null};
    },
    [fetchMovieDetails.fulfilled]: (state, action) => {
      return {...state, isLoading: false, error: null, movieDetails: action.payload};
    },
    [fetchMovieDetails.rejected]: (state, action) => {
      return {...state, isLoading: false, error: "Something went wrong!"};
    },
  }
});

// export const { addDetails } = movieDetailsSlice.actions;
export const getDetails = (state) => state.details;
export default movieDetailsSlice.reducer;
